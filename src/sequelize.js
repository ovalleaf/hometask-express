const { Sequelize } = require("sequelize");

// Create a Sequelize instance with SQLite as the dialect
const sequelize = new Sequelize({
  dialect: "sqlite",
  storage: "../database.sqlite3", // Path to your SQLite database file
});

console.log("sequelize: ", sequelize);

module.exports = { Sequelize, sequelize };
