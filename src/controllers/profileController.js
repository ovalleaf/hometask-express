const { Op } = require("sequelize");

// Get the top-paid clients within a specified date range
const getTopPaidClients = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get("models");
  const { start, end } = req.query;
  const { limit = 2 } = req.query;

  try {
    const jobs = await Job.findAll({
      where: {
        paymentDate: {
          [Op.and]: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        paid: true,
      },
    });

    const moneyPerClient = {};

    for (const job of jobs) {
      const contract = await Contract.findOne({
        where: {
          id: job.ContractId,
        },
      });

      const user = await Profile.findOne({
        where: {
          id: contract.ClientId,
        },
      });

      if (!moneyPerClient[user.id]) {
        moneyPerClient[user.id] = {
          paid: 0,
          fullName: `${user.firstName} ${user.lastName}`,
        };
      }

      moneyPerClient[user.id].paid += job.price;
    }

    const clients = Object.values(moneyPerClient);

    // Sort clients by paid amount in descending order
    clients.sort((a, b) => b.paid - a.paid);

    // Get the top-paid clients up to the specified limit
    const topPaid = clients.slice(0, limit);

    res.json(topPaid);
  } catch (error) {
    console.error("Error fetching top-paid clients:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Deposit money into the balance of a client
const depositBalance = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get("models");
  const { userId } = req.params;
  const { amount } = req.body;

  try {
    const contracts = await Contract.findAll({
      where: {
        ClientId: req.profile.id,
        status: { [Op.ne]: "terminated" },
      },
    });

    const jobs = await Job.findAll({
      where: {
        ContractId: { [Op.in]: contracts.map((c) => c.id) },
        paid: { [Op.eq]: null },
      },
    });

    const totalUnpaidJobs = jobs.reduce((total, job) => {
      return total + job.price;
    }, 0);

    const minimumAmountForJobs = +Number(totalUnpaidJobs * 0.25).toFixed(2);

    if (minimumAmountForJobs > 0 && amount > minimumAmountForJobs) {
      return res.status(403).json({
        error: `You can't deposit more than 25% of the total value of your unpaid jobs. You currently have R${totalUnpaidJobs} worth of unpaid jobs, and 25% of that is R${minimumAmountForJobs}.`,
      });
    }

    await Profile.increment("balance", {
      by: amount,
      where: {
        id: userId,
      },
    });

    res.json(
      `R${amount} deposited to user ${userId} by client ${req.profile.id}`
    );
  } catch (error) {
    console.error("Error depositing money:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

module.exports = {
  getTopPaidClients,
  depositBalance,
};
