const { Op } = require("sequelize");

// Get all unpaid jobs for a user (either a client or contractor) for active contracts
const getUnpaidJobs = async (req, res) => {
  const { Job, Contract } = req.app.get("../models/");

  try {
    const contracts = await Contract.findAll({
      where: {
        [Op.or]: [
          { ClientId: req.profile.id },
          { ContractorId: req.profile.id },
        ],
        status: { [Op.ne]: "terminated" },
      },
    });

    const jobs = await Job.findAll({
      where: {
        ContractId: { [Op.in]: contracts.map((c) => c.id) },
        paid: { [Op.eq]: null },
      },
    });

    res.json(jobs);
  } catch (error) {
    console.error("Error fetching unpaid jobs:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Pay for a job
const payForJob = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get("models");
  const { job_id } = req.params;

  try {
    const job = await Job.findOne({
      where: {
        id: job_id,
      },
    });

    if (!job) {
      return res.status(404).end(`Job ${job_id} not found`);
    }

    if (req.profile.balance < job.price) {
      return res
        .status(404)
        .end(
          `Insufficient funds to pay for the job. It costs R${job.price}, but the user has R${req.profile.balance}`
        );
    }

    const contract = await Contract.findOne({
      where: {
        id: job.ContractId,
      },
    });

    await Profile.increment("balance", {
      by: job.price,
      where: {
        id: contract.ContractorId,
      },
    });

    await Profile.decrement("balance", {
      by: job.price,
      where: {
        id: req.profile.id,
      },
    });

    await Job.update(
      {
        paid: true,
        paymentDate: new Date().toISOString(),
      },
      {
        where: {
          id: job_id,
        },
      }
    );

    res.json(
      `R${job.price} paid for the job to contractor ${contract.ContractorId} by client ${req.profile.id}`
    );
  } catch (error) {
    console.error("Error paying for job:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Get the best-paying profession within a given date range
const getBestProfession = async (req, res) => {
  const { Job, Contract, Profile } = req.app.get("models");
  const { start, end } = req.query;

  try {
    const jobs = await Job.findAll({
      where: {
        paymentDate: {
          [Op.and]: {
            [Op.gte]: start,
            [Op.lte]: end,
          },
        },
        paid: true,
      },
    });

    const moneyPerProfession = {};

    for (const job of jobs) {
      const contract = await Contract.findOne({
        where: {
          id: job.ContractId,
        },
      });

      const user = await Profile.findOne({
        where: {
          id: contract.ClientId,
        },
      });

      if (!moneyPerProfession[user.profession]) {
        moneyPerProfession[user.profession] = 0;
      }

      moneyPerProfession[user.profession] += job.price;
    }

    let currentMostPaid = {
      profession: null,
      amount: null,
    };

    for (const profession in moneyPerProfession) {
      if (
        !currentMostPaid.amount ||
        moneyPerProfession[profession] > currentMostPaid.amount
      ) {
        currentMostPaid = {
          profession: profession,
          amount: moneyPerProfession[profession],
        };
      }
    }

    return res.json(currentMostPaid);
  } catch (error) {
    console.error("Error retrieving data:", error);
    return res
      .status(500)
      .json({ error: "An error occurred while fetching data" });
  }
};

// Define other job-related controller functions as needed

module.exports = {
  getUnpaidJobs,
  payForJob,
  getBestProfession,
};
