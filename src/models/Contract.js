const { Sequelize, sequelize } = require("../sequelize");

class Contract extends Sequelize.Model {
  static initialize() {
    this.init(
      {
        terms: {
          type: Sequelize.TEXT,
          allowNull: false,
        },
        status: {
          type: Sequelize.ENUM("new", "in_progress", "terminated"),
        },
      },
      {
        sequelize,
        modelName: "Contract",
      }
    );
  }

  static associate(models) {
    // Associations with Profile model (Contractor and Client)
    this.belongsTo(models.Profile, {
      as: "Contractor",
      foreignKey: "ContractorId",
    });

    this.belongsTo(models.Profile, {
      as: "Client",
      foreignKey: "ClientId",
    });

    // Associations with Job model
    this.hasMany(models.Job);
  }
}

module.exports = Contract;
