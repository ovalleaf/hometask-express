const { Sequelize, sequelize } = require("../sequelize");

class Profile extends Sequelize.Model {
  static initialize() {
    this.init(
      {
        firstName: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        lastName: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        profession: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        balance: {
          type: Sequelize.DECIMAL(12, 2),
        },
        type: {
          type: Sequelize.ENUM("client", "contractor"),
        },
      },
      {
        sequelize,
        modelName: "Profile",
      }
    );
  }

  static associate(models) {
    // Associations with Contract model (as Contractor and Client)
    this.hasMany(models.Contract, {
      as: "ContractsAsContractor",
      foreignKey: "ContractorId",
    });

    this.hasMany(models.Contract, {
      as: "ContractsAsClient",
      foreignKey: "ClientId",
    });

    // Associations with Job model (as Client and Contractor)
    this.hasMany(models.Job, {
      as: "JobsAsClient",
      foreignKey: "ClientId",
    });

    this.hasMany(models.Job, {
      as: "JobsAsContractor",
      foreignKey: "ContractorId",
    });
  }
}

module.exports = Profile;
