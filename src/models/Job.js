const { Sequelize, sequelize } = require("../sequelize");

class Job extends Sequelize.Model {
  static initialize() {
    this.init(
      {
        description: {
          type: Sequelize.TEXT,
          allowNull: false,
        },
        price: {
          type: Sequelize.DECIMAL(12, 2),
          allowNull: false,
        },
        paid: {
          type: Sequelize.BOOLEAN,
          defaultValue: false, // Corrected to set a default value
        },
        paymentDate: {
          type: Sequelize.DATE,
        },
      },
      {
        sequelize,
        modelName: "Job",
      }
    );
  }

  static associate(models) {
    // Associations with Contract model
    this.belongsTo(models.Contract);

    // No need to specify associations with Profile model
  }
}

module.exports = Job;
