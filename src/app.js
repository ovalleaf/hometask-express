const express = require("express");
const app = express();
// const { sequelize, Op } = require("./models"); // Assuming you have a 'model' file with the Sequelize instance

const jobRouter = require("./routes/jobRoutes");
const contractRouter = require("./routes/contractRoutes");
const profileRouter = require("./routes/profileRoutes");

// Body parsing middleware
app.use(express.json());

// Mounting routes
app.use("/jobs", jobRouter);
app.use("/contracts", contractRouter);
app.use("/profiles", profileRouter);

module.exports = app;
