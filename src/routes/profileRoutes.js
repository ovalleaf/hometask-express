const express = require("express");
const { getProfile } = require("../middleware/getProfile");
const profileController = require("../controllers/profileController");

const router = express.Router();

// Get the top-paid clients within a specified date range
router.get(
  "/admin/best-clients",
  getProfile,
  profileController.getTopPaidClients
);

// Deposit money into the balance of a client
router.post(
  "/balances/deposit/:userId",
  getProfile,
  profileController.depositBalance
);

// Define other profile-related routes and controllers as needed

module.exports = router;
