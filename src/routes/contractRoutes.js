const express = require("express");
const { getProfile } = require("../middleware/getProfile");
const contractController = require("../controllers/contractController");

const router = express.Router();

// Get a list of contracts belonging to a user (client or contractor)
router.get("/contracts", getProfile, contractController.getContracts);
// Define other contract-related routes and controllers as needed

module.exports = router;
