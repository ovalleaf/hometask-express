const express = require("express");
const { getProfile } = require("../middleware/getProfile");
const jobController = require("../controllers/jobController");

const router = express.Router();

// Get all unpaid jobs for a user (either a client or contractor) for active contracts
router.get("/jobs/unpaid", getProfile, jobController.getUnpaidJobs);

// Pay for a job
router.post("/jobs/:job_id/pay", getProfile, jobController.payForJob);

// Define the route to get the best-paying profession
router.get(
  "/admin/best-profession",
  getProfile,
  jobController.getBestProfession
);

module.exports = router;
